#pragma once

#include <stddef.h>
#include <stdint.h>

#include <qnnpack/params.h>
#include <qnnpack/common.h>

#ifdef __cplusplus
extern "C" {
#endif

#define DECLARE_U8CLAMP_UKERNEL_FUNCTION(fn_name)   \
  QNNP_INTERNAL void fn_name(                       \
      size_t n,                                     \
      const uint8_t* x,                             \
      uint8_t* y,                                   \
      const union qnnp_u8_clamping_params* params);

DECLARE_U8CLAMP_UKERNEL_FUNCTION(u8clamp_ukernel__neon)
DECLARE_U8CLAMP_UKERNEL_FUNCTION(u8clamp_ukernel__sse2)

#ifdef __cplusplus
} /* extern "C" */
#endif
